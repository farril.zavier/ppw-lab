from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Farril Zavier Fernaldy' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000, 2, 6) #TODO Implement this, format (Year, Month, Date)
npm = 1706074631 # TODO Implement this
college = 'Fasilkom UI'
hobby1 = 'browsing web'
hobby2 = 'main game'
hobby3 = 'fotografi'
description1 = '6 Februari 2000'
description2 = 'New York'
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'college': college, 'hobby1': hobby1, 'hobby2': hobby2, 'hobby3': hobby3, 'description1': description1, 'description2': description2}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
